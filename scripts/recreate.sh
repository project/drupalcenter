#!/usr/bin/env bash

# Install Drupal 10 from config.
drush -y site:install --existing-config --account-pass=admin

NC='\033[0m' # No Color
On_Cyan='\033[46m'        # Cyan
echo -e "${On_Cyan}\n"
echo -e "${On_Cyan}  Successfully installed Drupal."
echo -e "${On_Cyan}"
echo -e "${On_Cyan}  The credentials for user one are admin:admin"
echo -e "${On_Cyan}  You can login at "`drush uli`
echo -e "${NC}\n"
