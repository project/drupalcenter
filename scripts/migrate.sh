#!/usr/bin/env bash

drush -y en dc_migrate

echo "migrate-import upgrade_d6_file"
drush -y migrate-import upgrade_d6_file

echo "migrate-import upgrade_d6_user_picture_file"
drush -y migrate-import upgrade_d6_user_picture_file

echo "migrate-import upgrade_d6_user"
drush -y migrate-import upgrade_d6_user

echo "migrate-import upgrade_d6_taxonomy_vocabulary"
drush -y migrate-import upgrade_d6_taxonomy_vocabulary

echo "migrate-import upgrade_d6_taxonomy_term"
drush -y migrate-import upgrade_d6_taxonomy_term

echo "migrate-import upgrade_d6_node_complete_forum"
drush -y migrate-import upgrade_d6_node_complete_forum

drush cr
