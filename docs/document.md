# Documentation

## Tools

The documentation is done with [MkDocs](https://www.mkdocs.org/) and [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)

The latest project documentation is automatically deployed via [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) at  
[http://project.pages.drupalcode.org/drupalcenter](http://project.pages.drupalcode.org/drupalcenter)

## Files and folder

The MkDocs configuration file is placed in the project's root and is called [mkdocs.yml](https://git.drupalcode.org/project/drupalcenter/-/blob/main/mkdocs.yml?ref_type=heads)

The documentation files are located in the [docs folder](https://git.drupalcode.org/project/drupalcenter/-/blob/main/mkdocs.yml?ref_type=heads).

To add additional pages, just add a [Markdown formatted](https://www.markdownguide.org/cheat-sheet/) file to the docs folder.

## Workflow

* Create an issue in the [issue queue](https://www.drupal.org/project/issues/search/drupalcenter)
* Post new pages or change existing ones by following [the development workflow](develop.md)
