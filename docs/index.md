
@todo A Drupalcenter mission statement.

## General project overview

The project page on d.o  
[https://www.drupal.org/project/drupalcenter](https://www.drupal.org/project/drupalcenter)

The project documentation  
[http://project.pages.drupalcode.org/drupalcenter](http://project.pages.drupalcode.org/drupalcenter)

The code repository  
[https://git.drupalcode.org/project/drupalcenter](https://git.drupalcode.org/project/drupalcenter)

The issue queue  
[https://www.drupal.org/project/issues/drupalcenter](https://www.drupal.org/project/issues/drupalcenter)

## Features

* @todo Aggregator
* @todo Forum
* @todo Handbook
* @todo Tutorials
* @todo Events
* @todo Follow
* @todo Mention
* @todo Notifications

## Tasks

* @todo Information architecture
* @todo Site Building
* @todo Design
* @todo Theme
* @todo Migration
