# Getting up & running

## Prerequisites

* We definitely need our preferred distributed versioning system [Git](https://git-scm.com/).  
See here for [installation instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

* And you might want to install DDEV for local development.  
See the [installation instructions](https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/)

**TIP:** If you choose to use DDEV you can absolutely skip the rest of the prerequisites section, everything is included there already.

* Basic requirements as documented on d.o.  
  See the [d.o requirements page](https://www.drupal.org/docs/getting-started/system-requirements/php-requirements)
* Composer is absolutely needed in a modern Drupal setup to ease package management - no way around that, [just get it](https://getcomposer.org/doc/00-intro.md).

**Take note: We are expecting `PHP 8.2` and `MariaDB 10.4` FTTB.**

## Getting the code

Clone the codebase into folder `drupalcenter`
```
git clone git@git.drupal.org:project/drupalcenter.git drupalcenter
```
Then enter the codebase `cd drupalcenter`. Looking around is like `ls -la`.

## Building the codebase
=== "CLI"

    ``` sh
    composer install
    ```

=== "DDEV"

    ``` sh
    ddev composer install
    ```

## Installing the setup

**Attention:** If you previously installed the setup, this command will delete everything and restart from scratch - no way back.

=== "CLI"

    ``` sh
    composer recreate
    ```

=== "DDEV"

    ``` sh
    ddev exec composer recreate
    ```

## Launching the site

**Note:** The credentials for user one are `admin`:`admin`.

=== "CLI"

    ``` sh
    ./vendor/bin/drush uli
    ```

=== "DDEV"

    ``` sh
    ddev drush uli
    ```

The above command gives you a link to your new site and logs you in automatically.  
Either `CTRL+c` and `CTRL+v` it into your browser's address bar or `CTRL+click` it to directly open the site in your default browser.

Welcome to the community 🤗
