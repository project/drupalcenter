# Development

## Development instance

Get your personal development instance preinstalled with everything needed to get into development mode at [Gitpod](https://www.gitpod.io/).

```
https://gitpod.io/#https://git.drupalcode.org/project/drupalcenter
```

You will need to register and authenticate against your d.o Gitlab account to proceed from there.
If successfull, this is what you get:

![Gitpod development instance](/assets/images/gitpod-development-instance.png)

## Identify yourself

```
@todo git config username, email
```
```
@todo authenticate with git.drupalcode.org
```

## The codebase

### Update the repository

Go to the console and run
=== "CLI"

    ``` sh
    git pull
    ```

=== "DDEV"

    ``` sh
    git pull
    ```

### Rebuild the codebase

Go to the console and run
=== "CLI"

    ``` sh
    composer update
    ```

=== "DDEV"

    ``` sh
    ddev composer update
    ```

### Import updated configuration

Go to the console and run
=== "CLI"

    ``` sh
    drush config:import
    ```

=== "DDEV"

    ``` sh
    ddev drush config:import
    ```

### Install from config

Go to the console and run
=== "CLI"

    ``` sh
    drush site:install --existing-config --account-pass=admin
    ```

=== "DDEV"

    ``` sh
    ddev drush site:install --existing-config --account-pass=admin
    ```

## The development workflow

### Issue creation
Create an issue in the [issue queue](https://www.drupal.org/project/issues/drupalcenter) describing the task your planning to work on:  

* [Create an issue now](https://www.drupal.org/node/add/project-issue/drupalcenter)
or
* [Find an existing issue](https://www.drupal.org/project/issues/drupalcenter)

### Issue fork creation
Create an issue fork to be able to work on your issue in isolation without affecting others until you finished.  
On your issue click on the green button labeled

* [Create issue fork]()

### Checkout issue fork
To be able to work on your issue fork you need to check it out.  
On your issue click on the blue link labeled

* [Show commands]()

You get the commands to check out the issue fork into yout workspace. e.g. Gitpod or local DDEV environment.

Those look like
```
git remote add drupalcenter-3479262 git@git.drupal.org:issue/drupalcenter-3479262.git
git fetch drupalcenter-3479262
```

Copy them `CTRL+c` one by one and past them `CTRL+v` nto your terminal.
This will add the issue fork as a new remote repository and check the files out.

You can now change, add or remove files by following a

### General Git workflow

Show the repository status (added, removed, changed files)
```
git status
```
Add a new file or a changed file
```
git add {filname}
```

Remove a file
```-=======
git rm {filname}
```

Undo changes to a file
```
git reset {filname}
```

Undo changes to the whole repository
```
git reset --hard
```

Push files to the remote repository
```
git push
```

After the push you can open a merge request in Gitlab by `CTRL+click`ing the link that appears in the terminal, e.g. 
