# Drupalcenter

## Getting up and running

Basic requirements as documented on d.o:
https://www.drupal.org/docs/getting-started/system-requirements/php-requirements

Also you might want to install DDEV for local development:
https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/

Get the code:
```
git clone git@git.drupal.org:project/drupalcenter.git drupalcenter
```

Build the codebase:
```
cd drupalcenter
composer install
```
or with DDEV:
```
cd drupalcenter
ddev composer install
```

Install the setup:
```
composer recreate 
```
or with DDEV
```
ddev exec composer recreate
```
